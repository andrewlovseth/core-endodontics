<section id="hero">

	<div class="photo cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">

	</div>

	<div class="info">
		<div class="info-wrapper">
			<h1><?php echo get_field('hero_headline'); ?></h1>
			<h2><?php echo get_field('hero_tagline'); ?></h2>
		</div>
	</div>

</section>