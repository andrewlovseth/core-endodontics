<div class="main-nav">

	<a href="<?php echo site_url('/'); ?>">Home</a>

	<?php if(have_rows('navigation', 'options')): while(have_rows('navigation', 'options')): the_row(); ?>
	 	<a href="<?php echo get_sub_field('link'); ?>"><?php echo get_sub_field('label'); ?></a>
	<?php endwhile; endif; ?>

</div>

<nav class="cta-nav mobile">

	<?php if(have_rows('ctas', 'options')): while(have_rows('ctas', 'options')): the_row(); ?>
		<div class="cta-btn">
		 	<a href="<?php echo get_sub_field('link'); ?>"><?php echo get_sub_field('label'); ?></a>
		 </div>
	<?php endwhile; endif; ?>

</nav>