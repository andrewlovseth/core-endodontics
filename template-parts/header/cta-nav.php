<nav class="cta-nav desktop">
    <div class="wrapper">
        
        <?php if(have_rows('ctas', 'options')): while(have_rows('ctas', 'options')): the_row(); ?>
            <div class="cta-btn">
                <a href="<?php echo get_sub_field('link'); ?>"><?php echo get_sub_field('label'); ?></a>
                </div>
        <?php endwhile; endif; ?>

    </div>
</nav>	
