<?php

    $mercer_island = get_field('mercer_island_office', 'options');
    $title = $mercer_island['title'];
    $phone = $mercer_island['phone'];
    $fax = $mercer_island['fax'];
    $address = $mercer_island['address'];
    $email = $mercer_island['email'];

?>

<div class="office mercer-island">
    <h4 class="sub-head"><?php echo $title; ?></h4>

    <p>
        <?php if($address): ?>
            <?php echo $address; ?><br/>
        <?php endif; ?>

        <?php if($phone): ?>
            <a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a><br/>
        <?php endif; ?>

        <?php if($email): ?>
            <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
        <?php endif; ?>
    </p>
</div>