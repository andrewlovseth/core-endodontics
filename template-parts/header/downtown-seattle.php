<?php

    $downtown = get_field('downtown_seattle_office', 'options');
    $title = $downtown['title'];
    $phone = $downtown['phone'];
    $fax = $downtown['fax'];
    $address = $downtown['address'];
    $email = $downtown['email'];

?>

<div class="office downtown-seattle">
    <h4 class="sub-head"><?php echo $title; ?></h4>
    
    <p>
        <?php if($address): ?>
            <?php echo $address; ?><br/>
        <?php endif; ?>

        <?php if($phone): ?>
            <a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a><br/>
        <?php endif; ?>

        <?php if($email): ?>
            <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
        <?php endif; ?>
    </p>
</div>