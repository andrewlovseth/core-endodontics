<section class="referral grid">
	
	<div class="info">
		<h4>Have a patient?</h4>

		<div class="btn-wrapper">
			<a class="btn" href="<?php echo site_url('/patient-referral/'); ?>">Send us a referral</a>
		</div>
	</div>

</section>