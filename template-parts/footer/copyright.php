<?php
    $copyright = get_field('copyright','options');
?>

<div class="copyright">
    <p><?php echo $copyright; ?></p>
</div>