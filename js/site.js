(function ($, window, document, undefined) {

	$(document).ready(function() {

		// rel="external"
		$('a[rel="external"]').click( function() {
			window.open( $(this).attr('href') );
			return false;
		});

		// Menu Toggle
		$('#toggle').click(function(){
			$('header').toggleClass('open');
			$('nav#mobile-nav').slideToggle(300);
			return false;
		});

		// FAQs
		$('.faq .question').click(function(e) {
	  
		    var $this = $(this);
		  
		    if ($this.next().hasClass('show')) {
		        $this.next().removeClass('show');
		        $this.next().slideUp(350);
		    } else {
		        $this.parent().parent().find('.faq .answer').removeClass('show');
		        $this.parent().parent().find('.faq .answer').slideUp(350);
		        $this.next().toggleClass('show');
		        $this.next().slideToggle(350);
			    $('.faq .question').removeClass('active');
		    }

		    $this.toggleClass('active');

			return false;

		});

		
		// Logins Parent Link Disable
		$('.menu-item-525 > a').click(function(e) {
	  		return false;
		});

		$('.video-player').fitVids();

	});

})(jQuery, window, document);