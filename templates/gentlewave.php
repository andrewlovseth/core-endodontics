<?php

/*

	Template Name: Gentlewave Technology

*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>


    <section id="features">
        <div class="wrapper">

            <div class="section-header">
                <h2><?php echo get_field('features_section_header'); ?></h2>
            </div>

            <?php if(have_rows('features')): while(have_rows('features')): the_row(); ?>

                <div class="feature">
                    <div class="icon">
                        <img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </div>

                    <div class="info">
                        <div class="headline">
                            <h3><?php echo get_sub_field('headline'); ?></h3>
                        </div>

                        <div class="copy">
                            <?php echo get_sub_field('copy'); ?>
                        </div>                        
                    </div>            
                </div>

            <?php endwhile; endif; ?>

            <div class="footnotes">
                <p><?php echo get_field('features_footnotes'); ?></p>
            </div>        
        
        </div>
    </section>

    <section id="video">
        <div class="wrapper">

            <div class="section-header">
                <h2><?php echo get_field('video_section_header'); ?></h2>
            </div>

            <div class="video-player">
                <?php echo get_field('video_embed'); ?>
            </div>
            
        </div>
   </section>


<?php get_footer(); ?>