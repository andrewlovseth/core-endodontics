<section id="mark-germack">
    <div class="photo">
        <img src="<?php $image = get_field('germack_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
    </div>

    <div class="info">
        <h2><?php echo get_field('germack_name'); ?></h2>
        <h5><?php echo get_field('germack_title'); ?></h5>
        <?php echo get_field('germack_bio'); ?>
    </div>
</section>