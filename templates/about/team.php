<?php if(have_rows('team')): ?>

    <section id="team">
        <div class="section-header">
            <h3 class="goldenrod">Our Team</h3>
        </div>

        <?php while(have_rows('team')): the_row(); ?>

            <div class="team-member">
                <div class="photo">
                    <img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                </div>

                <div class="info">
                    <h4><?php echo get_sub_field('name'); ?></h4>
                    <h5><?php echo get_sub_field('title'); ?></h5>

                    <?php echo get_sub_field('bio'); ?>
                </div>
            </div>

        <?php endwhile; ?>
    </section>

<?php endif; ?>