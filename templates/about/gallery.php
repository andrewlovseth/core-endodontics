<?php $images = get_field('gallery'); if( $images ): ?>

    <section id="gallery">
        <?php
            $photoIDs = ' '; 
            foreach( $images as $image ) {
                $photoIDs .= $image['ID'] . ',';
            }
            
            echo do_shortcode('[gallery ids="' . $photoIDs . '"]');
        ?>
    </section>
    
<?php endif; ?>