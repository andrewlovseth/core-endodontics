
<?php if(have_rows('our_doctors')):  ?>

    <section id="our-doctors">
        <?php while(have_rows('our_doctors')): the_row(); ?>
            
            <div class="doctor">
                <div class="photo">
                    <?php $image = get_sub_field('photo'); if($image): ?>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    <?php endif; ?>
                </div>

                <div class="info">
                    <h2><?php echo get_sub_field('name'); ?></h2>
                    <h5><?php echo get_sub_field('title'); ?></h5>
                    <?php echo get_sub_field('bio'); ?>
                </div>
            </div>

        <?php endwhile; ?>
    </section>

<?php endif; ?>
