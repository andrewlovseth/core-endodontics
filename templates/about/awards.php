<div class="awards">
    <?php if(have_rows('germack_awards')): while(have_rows('germack_awards')): the_row(); ?>
        
        <div class="award">
            <img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        </div>

    <?php endwhile; endif; ?>
</div>