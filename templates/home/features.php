<section id="features">
    <div class="wrapper">

        <h3><span>Why CORE</span></h3>

        <div class="features-wrapper">

            <?php if(have_rows('features')): while(have_rows('features')): the_row(); ?>
                
                <div class="feature">
                    <div class="photo">
                        <a href="<?php echo get_sub_field('link'); ?>">
                            <img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                        </a>
                    </div>

                    <div class="info">
                        <h4><a href="<?php echo get_sub_field('link'); ?>"><?php echo get_sub_field('headline'); ?></a></h4>
                        <?php echo get_sub_field('deck'); ?>
                    </div>
                </div>

            <?php endwhile; endif; ?>

        </div>

    </div>
</section>
