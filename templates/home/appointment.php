<section id="appointment">
    <div class="wrapper">

        <h3><?php echo get_field('schedule_headline'); ?></h3>
        <p><?php echo get_field('schedule_subheadline'); ?></p>
        <a href="<?php echo site_url('/request-an-appointment/'); ?>" class="btn">Request an appointment</a>

    </div>
</section>
