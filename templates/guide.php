<?php

/*

	Template Name: Guide

*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>

	<section id="main">
		<div class="wrapper">

			<?php if(have_rows('guide')): while(have_rows('guide')) : the_row(); ?>
			 
			    <?php if( get_row_layout() == 'section' ): ?>
					
					<section class="phase">

						<div class="header">
							<h2 class="phase-title"><?php echo get_sub_field('title'); ?></h2>
						</div>

						<div class="body">

							<div class="narrative">
								<?php echo get_sub_field('narrative'); ?>
							</div>

							<div class="fact-box">
								<div class="fb-header">
									<h4><?php echo get_sub_field('fact_box_headline'); ?></h4>
								</div>

								<div class="fb-body">
									<?php echo get_sub_field('fact_box'); ?>
								</div>
							</div>

							<div class="faqs">

								<h3>FAQs</h3>
								
								<?php if(have_rows('faqs')): while(have_rows('faqs')): the_row(); ?>
								 
								    <div class="faq">
								    	<div class="question">
									        <h4><?php echo get_sub_field('question'); ?></h4>
									    </div>

									    <div class="answer">
									    	<?php echo get_sub_field('answer'); ?>
									    </div>
								    </div>

								<?php endwhile; endif; ?>

								<?php
									$documents_title = get_sub_field('documents_title');
									$documents_deck = get_sub_field('documents_deck');
								?>

								<?php if(have_rows('documents')): ?>

									<div class="documents">
										<h3><?php echo $documents_title; ?></h3>
										<?php echo $documents_deck; ?>

										<div class="document-list">
											<?php while(have_rows('documents')): the_row(); ?>

											    <div class="document">
											       <a href="<?php echo get_sub_field('file'); ?>"><?php echo get_sub_field('filename'); ?></a>
											    </div>

											<?php endwhile; ?>
										</div>
									</div>

								<?php endif; ?>

							</div>

						</div>

					</section>
					
			    <?php endif; ?>
			 
			<?php endwhile; endif; ?>


		</div>
	</section>

<?php get_footer(); ?>