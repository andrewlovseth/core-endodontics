<?php

/*

	Template Name: About

*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>

	<section id="main">
		<div class="wrapper">

			<?php get_template_part('templates/about/mark-germack'); ?>

			<?php get_template_part('templates/about/our-doctors'); ?>

			<?php get_template_part('templates/about/awards'); ?>

			<?php get_template_part('templates/about/team'); ?>

			<?php get_template_part('templates/about/gallery'); ?>

		</div>
	</section>

<?php get_footer(); ?>