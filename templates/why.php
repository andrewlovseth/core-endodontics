<?php

/*

	Template Name: Why

*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>

	<section id="main">
		<div class="wrapper">

			<section id="why-us">

				<div class="section-header">
					<h3 class="green">Why CORE Endodontics</h3>
				</div>

				<div class="info">
					<?php echo get_field('why'); ?>
				</div>

			</section>


			<section id="amenities">

				<div class="section-header">
					<h3 class="goldenrod">Amenities</h3>
				</div>

				<?php if(have_rows('amenities')): while(have_rows('amenities')): the_row(); ?>

					<div class="amenity">
						<div class="photo">
							<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>

						<div class="info">
							<h4><?php echo get_sub_field('name'); ?></h4>
							<?php echo get_sub_field('description'); ?>
						</div>
					</div>

				<?php endwhile; endif; ?>

			</section>


			<section id="technology">

				<div class="section-header">
					<h3 class="goldenrod">Technology</h3>
				</div>

				<?php if(have_rows('technology')): while(have_rows('technology')): the_row(); ?>

					<div class="technology">
						<div class="photo">
							<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>

						<div class="info">
							<h4><?php echo get_sub_field('name'); ?></h4>
							<?php echo get_sub_field('description'); ?>
						</div>
					</div>

				<?php endwhile; endif; ?>

			</section>


			<section id="press">

				<div class="section-header">
					<h3 class="goldenrod">Press</h3>
				</div>

				<?php if(have_rows('press')): while(have_rows('press')): the_row(); ?>

					<div class="press">
						<div class="photo">
							<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>

						<div class="info">
							<h4><?php echo get_sub_field('name'); ?></h4>
							<?php echo get_sub_field('description'); ?>
						</div>
					</div>

				<?php endwhile; endif; ?>


			</section>


		</div>
	</section>



<?php get_footer(); ?>