<?php

/*

	Template Name: Contact

*/

get_header(); ?>

	<section class="locations grid">

		<?php get_template_part('templates/contact/downtown-seattle'); ?>

		<?php get_template_part('templates/contact/mercer-island'); ?>

	</section>
	
	<?php get_template_part('templates/contact/form'); ?>

<?php get_footer(); ?>