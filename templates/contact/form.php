<section class="contact-form grid">
    <div class="section-header">
        <h3 class="dark-blue">Email Us</h3>
    </div>

    <?php echo do_shortcode('[wpforms id="128"]'); ?>
</section>