<?php

    $mercer_island = get_field('mercer_island_office', 'options');
    $title = $mercer_island['title'];
    $photo = $mercer_island['photo'];
    $phone = $mercer_island['phone'];
    $fax = $mercer_island['fax'];
    $address = $mercer_island['address'];
    $email = $mercer_island['email'];
    $google_maps = $mercer_island['google_map'];
    $directions = $mercer_island['directions'];

?>

<div class="office mercer-island">
    <h4 class="sub-head"><?php echo $title; ?></h4>

    <div class="photo">
        <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
    </div>

    <div class="info">

        <?php if($address): ?>
            <h5>Address</h5>
            <p><?php echo $address; ?></p>
        <?php endif; ?>

        <?php if($phone): ?>
            <h5>Phone</h5>
            <p><a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a></p>
        <?php endif; ?>

        <?php if($fax): ?>
            <h5>Fax</h5>
            <p><a href="tel:<?php echo $fax; ?>"><?php echo $fax; ?></a></p>
        <?php endif; ?>

        <?php if($email): ?>
            <h5>Email</h5>
            <p><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
        <?php endif; ?>


        <?php if($directions): ?>
            <h5>Directions</h5>
            <p class="directions">
                <?php echo $directions; ?>
                <?php if($google_maps): ?>
                    <br/><a class="google-maps" href="<?php echo $google_maps; ?>" target="window">Google Maps</a>
                <?php endif; ?>
            </p>
        <?php endif; ?>
    </div>


    
</div>