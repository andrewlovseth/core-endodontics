<?php

/*

	Template Name: Schedule

*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>

	<section id="main">
		<div class="wrapper">

			<section id="schedule-form">
				<div class="section-header">
					<h3 class="goldenrod">Request an appointment</h3>
				</div>

				<section class="copy">
					<?php echo get_field('schedule_info'); ?>
				</section>

				<?php echo do_shortcode('[wpforms id="144"]'); ?>

			</section>


		</div>
	</section>



<?php get_footer(); ?>