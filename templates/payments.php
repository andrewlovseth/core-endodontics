<?php

/*

	Template Name: Payments & Insurance

*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>

	<section id="about-statement">
		<div class="wrapper">

			<?php echo get_field('about_statement'); ?>

		</div>
	</section>

	<section id="main">
		<div class="wrapper">

			<section id="insurance-info" class="copy">
				<div class="section-header">
					<h3 class="goldenrod">Insurance Info</h3>
				</div>

				<?php echo get_field('insurance_info'); ?>
			</section>

		</div>
	</section>

<?php get_footer(); ?>