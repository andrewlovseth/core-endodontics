<?php

/*

	Template Name: Referrals

*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>

	<section id="about-statement" class="copy">
		<div class="wrapper">

			<?php echo get_field('about_statement'); ?>

		</div>
	</section>

	<section id="main">
		<div class="wrapper">

			<?php echo do_shortcode('[wpforms id="167"]'); ?>


		</div>
	</section>

<?php get_footer(); ?>