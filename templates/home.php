<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>

	<?php get_template_part('templates/home/about'); ?>

	<?php get_template_part('templates/home/appointment'); ?>

	<?php get_template_part('templates/home/features'); ?>

<?php get_footer(); ?>