	<?php get_template_part('template-parts/footer/referral'); ?>

	<footer class="site-footer grid">

		<?php get_template_part('template-parts/footer/logo'); ?>
		
		<?php get_template_part('template-parts/global/nav-links'); ?>

		<?php get_template_part('template-parts/header/downtown-seattle'); ?>

		<?php get_template_part('template-parts/header/mercer-island'); ?>

		<?php get_template_part('template-parts/footer/copyright'); ?>

	</footer>
		
	<?php wp_footer(); ?>

</body>
</html>