const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();

function style() {
    // Main Stylesheet
    return gulp.src('sass/style.scss').pipe(sass().on('error', sass.logError)).pipe(autoprefixer()).pipe(sourcemaps.write()).pipe(gulp.dest('./')).pipe(browserSync.stream());
}

function watch() {
    browserSync.init({
        proxy: 'https://core-endo.local',
    });

    gulp.watch('./sass/**/*.scss', style);
    gulp.watch('./partials/**/*.php').on('change', browserSync.reload);
    gulp.watch('./*.php').on('change', browserSync.reload);
    gulp.watch('./js/**/*.js').on('change', browserSync.reload);
}

exports.style = style;
exports.watch = watch;
