<?php

/*

    ----------------------------------------------------------------------
    					XX Theme Support
    ----------------------------------------------------------------------

*/
add_theme_support( 'title-tag' );
show_admin_bar( false );

function register_my_menu() {
  register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_my_menu' );

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function custom_tiled_gallery_width() {
    return '1200';
}
add_filter( 'tiled_gallery_content_width', 'custom_tiled_gallery_width' );

// Enqueue custom styles and scripts
function enqueue_styles_and_scripts() {
    // Register and noConflict jQuery 3.4.1
    wp_register_script( 'jquery.3.4.1', 'https://code.jquery.com/jquery-3.4.1.min.js' );
    wp_add_inline_script( 'jquery.3.4.1', 'var jQuery = $.noConflict(true);' );

    $uri = get_stylesheet_directory_uri();
    $dir = get_stylesheet_directory();

    $script_last_updated_at = filemtime($dir . '/js/site.js' );
    $style_last_updated_at = filemtime($dir . '/style.css' );

    // Add style.css
    wp_enqueue_style( 'adobe-fonts', 'https://use.typekit.net/zqe8frk.css' );
    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Montserrat:300,400,700&display=swap' );
    wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/style.css' , '', $style_last_updated_at);

    // Add plugins.js & site.js (with jQuery dependency)
    wp_enqueue_script( 'custom-plugins', get_stylesheet_directory_uri() . '/js/plugins.js', array( 'jquery.3.4.1' ), $script_last_updated_at, true );
    wp_enqueue_script( 'custom-site', get_stylesheet_directory_uri() . '/js/site.js', array( 'jquery.3.4.1' ), $script_last_updated_at, true );
}
add_action( 'wp_enqueue_scripts', 'enqueue_styles_and_scripts' );




/*

    ----------------------------------------------------------------------
    					XX Code Cleanup
    ----------------------------------------------------------------------

*/

remove_action( 'wp_head', 'wp_resource_hints', 2 );
remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action( 'wp_head', 'wp_generator');
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}
 
// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );
 
// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );






/*

    ----------------------------------------------------------------------
    					XX Custom Functions
    ----------------------------------------------------------------------

*/

/* Filter <p>'s on <img> and <iframe>' */
function filter_ptags_on_images($content) {
	$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	return preg_replace('/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU', '\1', $content);
}
add_filter('the_content', 'filter_ptags_on_images');
add_filter('acf/format_value/type=wysiwyg', 'filter_ptags_on_images', 10, 3);


/* List of Tag Slugs */
function entry_tags() {
	$posttags = get_the_tags();
	if ($posttags) {
	  foreach($posttags as $tag) {
	    echo $tag->slug; 
	  }
	}
}

function replace_admin_menu_icons_css() {
    ?>
    <style>

		.dashicons-admin-post:before,
		.dashicons-format-standard:before {
		    content: "\f331";
		}

    </style>
    <?php
}






/*

    ----------------------------------------------------------------------
    					XX Advanced Custom Fields
    ----------------------------------------------------------------------

*/

function my_relationship_query( $args, $field, $post_id ) {
    $args['orderby'] = 'date';
    $args['order'] = 'DESC';
    return $args;
}


// filter for every field
add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);


if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

if( function_exists('acf_add_options_sub_page') ) {
	acf_add_options_sub_page('Header');
	acf_add_options_sub_page('Footer');
}

function my_acf_admin_head() {
	?>
	<style type="text/css">

		.acf-relationship .list {
			height: 400px;
		}

	</style>

	<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');


// Disable author pages
function disable_author_pages() {
    if (is_author()) {
        global $wp_query;
        $wp_query->set_404();
        status_header(404);
        // Redirect to homepage or another page
        wp_redirect(home_url());
        exit;
    }
}
add_action('template_redirect', 'disable_author_pages');
