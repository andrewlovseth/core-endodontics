<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> id="<?php echo $post->post_name; ?>">

	<?php get_template_part('template-parts/header/navigation'); ?>

	<header class="site-header grid">

			<div class="logo">
				<?php get_template_part('template-parts/header/logo'); ?>
				
				<?php get_template_part('template-parts/header/hamburger'); ?>
			</div>

			<?php get_template_part('template-parts/header/downtown-seattle'); ?>

			<?php get_template_part('template-parts/header/mercer-island'); ?>
	</header>
	
	<?php get_template_part('template-parts/header/cta-nav'); ?>

	<?php get_template_part('template-parts/header/mobile-nav'); ?>
